import React, { useState } from 'react';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import './dnd.css'
import garyImg from './images/gary.png';
import catoImg from './images/cato.png';
import kvnImg from './images/kvn.png';
import mooncakeImg from './images/mooncake.png';
import quinnImg from './images/quinn.png';

const finalSpaceCharacters = [
    {
        id: 'gary',
        name: 'Gary Goodspeed',
        thumb: garyImg
    },
    {
        id: 'cato',
        name: 'Little Cato',
        thumb: catoImg
    },
    {
        id: 'kvn',
        name: 'KVN',
        thumb: kvnImg
    },
    {
        id: 'mooncake',
        name: 'Mooncake',
        thumb: mooncakeImg
    },
    {
        id: 'quinn',
        name: 'Quinn Ergon',
        thumb: quinnImg
    }
]

function ReactBeautifulDnd() {
    const [characters, updateCharacters] = useState(finalSpaceCharacters)

    function handleOnDragEnd(result) {
           if (!result.destination) return;
           const items = Array.from(characters);
           const [reorderedItem] = items.splice(result.source.index, 1);
           items.splice(result.destination.index, 0, reorderedItem);

           updateCharacters(items);
    }


    return (
        <div className="dnd-container">
            <h1>Final Space Characters</h1>

            <DragDropContext onDragEnd={handleOnDragEnd}>
                <Droppable droppableId="characters">
                    {(provided) => (
                        <ul className="characters" {...provided.droppableProps} ref={provided.innerRef}>
                            {characters.map(({ id, name, thumb }, index) => (
                                <Draggable key={id} draggableId={id} index={index}>
                                    {(provided) => (
                                        <li
                                            ref={provided.innerRef}
                                            {...provided.draggableProps}
                                            {...provided.dragHandleProps}
                                        >
                                            <div className="characters-thumb">
                                                <img src={thumb} alt={`${name} Thumb`} />
                                            </div>
                                            <p>
                                                {name}
                                            </p>
                                        </li>
                                    )}
                                </Draggable>
                            ))}
                            {provided.placeholder}
                        </ul>
                    )}
                </Droppable>
            </DragDropContext>
            <p>
                Images from <a href="https://final-space.fandom.com/wiki/Final_Space_Wiki">Final Space Wiki</a>
            </p>
        </div>
    );
}

export default ReactBeautifulDnd;