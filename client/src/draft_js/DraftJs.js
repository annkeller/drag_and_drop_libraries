import React, { Component } from 'react';
import Heading from './component/Heading';

class DraftJs extends Component {
    render() {
        return (
            <div style={{ maxWidth: "700px", margin: "0 auto" }}>
                <a href="https://draftjs.org/" target="_blank" rel="noreferrer">
                    <h1>Draf.js</h1>
                </a>
                <h4>Rich Text Editor Framework for React</h4>
                <Heading />
            </div>
        );
    }
}

export default DraftJs;