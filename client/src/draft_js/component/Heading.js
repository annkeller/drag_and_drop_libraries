import React, { Component } from 'react';
import { Editor, EditorState, RichUtils, getDefaultKeyBinding } from "draft-js";
import { BlockStyleControls, InlineStyleControls, styleMap, getBlockStyle } from './CustomStyle';
import { stateToHTML } from 'draft-js-export-html';
import './richEditor.css'


class Heading extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editorState: EditorState.createEmpty(),
      preview: false,
      exportHtml: false,
    };

    this.focus = () => this.refs.editor.focus();
    this.onChange = (editorState) => this.setState({ editorState });

    this.handleKeyCommand = this._handleKeyCommand.bind(this);
    this.mapKeyToEditorCommand = this._mapKeyToEditorCommand.bind(this);
    this.toggleBlockType = this._toggleBlockType.bind(this);
    this.toggleInlineStyle = this._toggleInlineStyle.bind(this);
    this.togglePreview = this.togglePreview.bind(this)
    this.toggleExportHtml = this.toggleExportHtml.bind(this)
  }


  togglePreview() {
    this.setState({
      preview: !this.state.preview
    })
  }

  toggleExportHtml() {
    this.setState({
      exportHtml: !this.state.exportHtml
    })
  }

  _handleKeyCommand(command, editorState) {
    const newState = RichUtils.handleKeyCommand(editorState, command);
    if (newState) {
      this.onChange(newState);
      return true;
    }
    return false;
  }

  _mapKeyToEditorCommand(e) {
    if (e.keyCode === 9 /* TAB */) {
      const newEditorState = RichUtils.onTab(
        e,
        this.state.editorState,
        4, /* maxDepth */
      );
      if (newEditorState !== this.state.editorState) {
        this.onChange(newEditorState);
      }
      return;
    }
    return getDefaultKeyBinding(e);
  }

  _toggleBlockType(blockType) {
    this.onChange(
      RichUtils.toggleBlockType(
        this.state.editorState,
        blockType
      )
    );
  }

  _toggleInlineStyle(inlineStyle) {
    this.onChange(
      RichUtils.toggleInlineStyle(
        this.state.editorState,
        inlineStyle
      )
    );
  }

  render() {
    const { editorState } = this.state;

    // If the user changes block type before entering any text, we can
    // either style the placeholder or hide it. Let's just hide it now.
    let className = 'RichEditor-editor';
    var contentState = editorState.getCurrentContent();
    if (!contentState.hasText()) {
      if (contentState.getBlockMap().first().getType() !== 'unstyled') {
        className += ' RichEditor-hidePlaceholder';
      }
    }

    let html = stateToHTML(contentState);

    return (
      <div>
        <button className="btn" onClick={this.togglePreview}>Preview</button>
        <button className="btn" onClick={this.toggleExportHtml}>Export HTML</button>

        <div className={`RichEditor-root${this.state.preview ? ' preview' : ''}`}>

          <div className={`${this.state.preview ? 'display-none' : ''}`}>
            <BlockStyleControls
              editorState={editorState}
              onToggle={this.toggleBlockType}
            />
            <InlineStyleControls
              editorState={editorState}
              onToggle={this.toggleInlineStyle}
            />
          </div>

          <div className={`${className}${this.state.preview ? ' preview' : ''}`} onClick={this.focus}>
            <Editor
              blockStyleFn={getBlockStyle}
              customStyleMap={styleMap}
              editorState={editorState}
              handleKeyCommand={this.handleKeyCommand}
              keyBindingFn={this.mapKeyToEditorCommand}
              onChange={this.onChange}
              placeholder="Heading..."
              ref="editor"
              spellCheck={true}
              textAlign={true}
            />
          </div>
        </div>

        {this.state.exportHtml ?
          <div className="export-container">
            {html}
            <div className="copy-box">
            <button
              onClick={() => navigator.clipboard.writeText(html)}
            >
              Copy
            </button>
            </div>
          </div>
          : null}

      </div>
    );
  }
}


export default Heading;