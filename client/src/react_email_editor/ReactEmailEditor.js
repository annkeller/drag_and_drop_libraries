import React, { Component, createRef } from 'react';
import EmailEditor from 'react-email-editor';
import sample from './sample.json'

class ReactEmailEditor extends Component {
    constructor(props) {
        super(props)
        this.emailEditorRef = createRef(null);
        this.onLoad = this.onLoad.bind(this)
        this.exportHtml = this.exportHtml.bind(this)
        this.saveDesign = this.saveDesign.bind(this)
        this.onDesignLoad = this.onDesignLoad.bind(this)

    }
    exportHtml() {
        this.emailEditorRef && this.emailEditorRef.current.editor.exportHtml((data) => {
            const { html } = data;
            console.log("exportHtml", html);
            alert("Output HTML has been logged in your developer console.");
        });
    };

    saveDesign() {
        this.emailEditorRef && this.emailEditorRef.current.editor.saveDesign((design) => {
            console.log("saveDesign", JSON.stringify(design, null, 4));
            alert("Design JSON has been logged in your developer console.");
        });
    };

    onDesignLoad(data) {
        console.log("onDesignLoad", data);
    };

    onLoad() {
        console.log('onLoad', this.emailEditorRef)
        if (this.emailEditorRef.current === null) {
            return null
        } else {
            this.emailEditorRef && this.emailEditorRef.current.editor.addEventListener(
                "onDesignLoad",
                this.onDesignLoad
            )
            this.emailEditorRef && this.emailEditorRef.current.editor.loadDesign(sample);
        }
    }


    render() {
        return (
            <div>
                <div>
                <button onClick={this.onLoad}>Load Design</button>
                    <button onClick={this.saveDesign}>Save Design</button>
                    <button onClick={this.exportHtml}>Export HTML</button>
                </div>
            
                <EmailEditor ref={this.emailEditorRef} onLoad={this.onLoad} minHeight="90vh" />
            </div>
        );
    }
}

export default ReactEmailEditor;