import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import ReactEmailEditor from './react_email_editor/ReactEmailEditor';
import ReactBeautifulDnd from './react_beautiful_dnd/ReactBeautifulDnd';
import DraftJs from './draft_js/DraftJs';
import './App.css';

export default class App extends Component {
  render() {

    return (
      <Router>
        <div className="App">
          <header className="App-header">
            <Link to="/">React Email Editor</Link>
            <Link to="/react-beautiful-dnd">React Beautiful DnD</Link>
            <Link to="/draft-js">Draft JS</Link>
          </header>

          <Switch>
            <Route exact path="/" component={ReactEmailEditor} />
            <Route exact path="/react-beautiful-dnd" component={ReactBeautifulDnd} />
            <Route path="/draft-js" component={DraftJs} />
          </Switch>
        </div>
      </Router>
    );
  }
}

