# README #
[Demo](https://demo-drag-and-drop.netlify.app/)

### Research React Drag and Drop Libraries ###

* [React Email Editor](https://github.com/unlayer/react-email-editor) is a embeddable drag & drop editor for SaaS.
* [React Beautiful DnD](https://openbase.com/js/react-beautiful-dnd) is a React drag and drop library.
* [Draft-JS](https://draftjs.org/) is a rich text editor for React.

### Set up ###

* cd `client`
* run `yarn`
* run `yarn start`
